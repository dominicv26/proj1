# README #

### Simple Web Server ###
Simple python web server. 
Author: Dominic Vicharelli, dominicv@uoregon.edu

  
* Deployment should work "out of the box" with this command sequence: 

  ```
  git clone https://dominicv26@bitbucket.org/dominicv26/proj1.git <targetDirectory>
  ```
  
  ```
  cd <targetDirectory>
  ```
  
  ```
  make run or make start
  ```
  
  *test it with a browser now, while your server is running in a background process*

  ```
  make stop
  ``` 

